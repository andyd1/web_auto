# -*- utf-8 -*-
# @Time:2021/4/23 0023 19:41
# @Author: zhaowangfa
# @file test_invest.py
# 独立的账号
# 投资用例
# 前置条件：
# 1、用户已登录；
# 2、有能够投的标，如果没有标，则先加标，通过接口加；
# 3、用户有余额投标

# 步骤：1、在首页选标 ---   默认第一个标；
# 2、标页面--输入投入金额、点击投资按钮；
# 3、标页面--点击投标成功的弹出框 -- 查看并激活，进入个人页面

# 断言：
# 钱 --投资后金额，是否少了投资的钱。
# 个人页面  -获取投资后的金额
# 投资前的金额  - 投资后的金额 = 投资金额
# 投资记录对不对
import unittest


class TestInvest(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_invest_success(self):
        # 步骤：1、在首页选标 ---   默认第一个标；
        # 2、标页面--输入投入金额、点击投资按钮；
        # 3、标页面--点击投标成功的弹出框 -- 查看并激活，进入个人页面

        # 断言：
        # 钱 --投资后金额，是否少了投资的钱。
        # 个人页面  -获取投资后的金额
        # 投资前的金额  - 投资后的金额 = 投资金额
        # 投资记录对不对
        pass

    # 投资金额非100整数倍
    def test_invest_failed_no100(self):
        pass

    # 投资金额非10整数倍
    def test_invest_failed_no10(self):
        pass
