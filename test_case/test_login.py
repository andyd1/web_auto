# -*- utf-8 -*-
# @Time:2021/4/22 0022 22:54
# @Author: zhaowangfa
# @file test_login.py
# 登录用例
import unittest
from selenium import webdriver
from page_actions.login_page import LoginPage
from page_actions.index_page import IndexPage
from test_data import common_data as cd
from test_data import login_data as ld
from ddt import ddt, data
import pytest


@ddt
class TestLogin(unittest.TestCase):

    @classmethod
    def setUpClass(cls):  # 每个类执行一次，就是下面的用例全部执行完
        cls.driver = webdriver.Chrome()
        cls.driver.get(cd.web_login_url)
        cls.driver.maximize_window()
        cls.lg = LoginPage(cls.driver)

    @classmethod
    def tearDownClass(cls):  # 每个类执行一次，就是下面的用例全部执行完
        cls.driver.quit()

    def setUp(self):  # 每个用例执行一次，就是下面每执行一条用例就执行一次
        pass

    def tearDown(self):  # 每个用例执行一次，就是下面每执行一条用例就执行一次
        # 用例后置
        self.driver.refresh()

    # 正常用例---登录成功
    @pytest.mark.smoke
    def test_login_2_success(self):
        # 前置  访问登录页面
        # 步骤  输入用户名：  密码：  点击登录
        self.lg.login(ld.success_data['username'], ld.success_data['password'])
        # 断言  首页当中是否找到 ‘退出’ 这个元素
        self.assertTrue(IndexPage(self.driver).is_exit_logout_ele())

    # 异常用例---手机号码不正确
    @data(*ld.error_phone)
    def test_login_1_error_ph(self, item):
        # 前置  访问登录页面
        # 步骤   输入用户名：  密码：  点击登录
        self.lg.login(item['username'], item['password'])
        # 断言  登录页提示：请输入正确的手机号码
        # 登录页面中 - 获取提示框的文本内容
        # 对比文本内容与期望值是否相等
        self.assertEqual(self.lg.get_login_error_info(), item['check'])

    # 异常用例--- 账号不存在或密码错误
    @data(*ld.error_data)
    def test_login_0_error_data(self, item):
        # 前置  访问登录页面
        # 步骤   输入用户名：  密码：  点击登录
        self.lg.login(item['username'], item['password'])
        # 断言  获取页面的弹出框提示
        # 对比文本内容与期望值是否相等
        self.assertEqual(self.lg.get_error_center_msg(), item['check'])
