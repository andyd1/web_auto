# -*- utf-8 -*-
# @Time:2021/4/24 0024 21:55
# @Author: zhaowangfa
# @file conftest.py
from selenium import webdriver
import pytest
from page_actions.login_page import LoginPage
driver = None


@pytest.fixture(scope='class')
def access_web():
    global driver
    driver = webdriver.Chrome()
    driver.get('')
    driver.maximize_window()
    lg = LoginPage(driver)
    yield driver, lg
    driver.quit()


@pytest.fixture
def refresh_page():
    global driver

    yield

    pass
