# -*- utf-8 -*-
# @Time:2021/4/24 0024 11:00
# @Author: zhaowangfa
# @file base_page.py
# 封装基本函数 - 执行日志、异常处理、失败截图
# 所有页面的公共部分
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
import logging
from tools.get_path import *


class BasePage:

    def __init__(self, driver):
        self.driver = driver

    # 等待元素可见
    def wait_ele_visible(self, locator, time=30, poll_frequency=0.5, doc=''):
        """
        :param locator:
        :param time:
        :param poll_frequency:
        :param doc:
        :return:
        """
        logging.info('等待元素{0}可见'.format(locator))
        try:
            # 等待的开始时间
            start = datetime.datetime.now()
            WebDriverWait(self.driver, time, poll_frequency).until(ec.visibility_of_element_located(locator))
            # 等待的结束时间
            end = datetime.datetime.now()
            # 等待了多久
            time_dif = start - end
            logging.info('等待时长为{0}'.format(time_dif))
        except Exception as e:
            logging.exception('等待元素{0}可见失败！！！'.format(locator))
            self.save_screenshots(doc)
            raise e

    # 等待元素存在
    def wait_ele_presence(self):
        pass

    # 查找元素
    def get_element(self, locator, doc=''):
        logging.info("查找元素{0}".format(locator))
        try:
            return self.driver.find_element(*locator)
        except Exception as e:
            logging.exception('查找不对元素{0}'.format(locator))
            self.save_screenshots(doc)
            raise e

    # 点击元素
    def click_element(self, locator, doc=''):
        # 找元素
        ele = self.get_element(locator, doc)
        logging.info('')
        try:
            ele.click()
        except Exception as e:
            logging.exception('元素{0}点击失败'.format(locator))
            self.save_screenshots(doc)
            raise e

    # 输入操作
    def input_text(self, locator, text, doc=''):
        # 找元素
        ele = self.get_element(locator, doc)
        logging.info('')
        try:
            ele.send_keys(text)
        except Exception as e:
            logging.exception('元素输入操作失败')
            self.save_screenshots(doc)
            raise e
        pass

    # 获取元素文本内容
    def get_element_text(self, locator, doc=""):
        # 找元素
        ele = self.get_element(locator, doc)
        try:
            return ele.text
        except Exception as e:
            logging.exception('获取元素文本内容失败')
            self.save_screenshots(doc)
            raise e

    # 获取元素属性
    def get_element_attribute(self):
        pass

    # assert处理
    def assert_action(self, action='accept'):
        pass

    # iframe的切换
    def switch_iframe(self, iframe_reference):
        pass

    # 上传操作
    def upload_file(self, file):
        pass

    # 滚动条处理
    # 窗口切换

    # 截图
    def save_screenshots(self, name):
        # 图片名称：模块名称_页面名称_操作名称_年-月-日-时分秒.png
        time1 = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        file_name = screenshot_path + '/{0}_{1}'.format(name, time1) + '.png'
        self.driver.save_screenshot(file_name)
