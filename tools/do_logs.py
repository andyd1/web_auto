# -*- utf-8 -*-
# @Time:2021/4/25 0025 9:53
# @Author: zhaowangfa
# @file do_logs.py
# 日志处理
import logging
from tools.get_path import *


class Dolog:

    def my_logs(self, message, level):
        # logger是收集信息
        # handler是输出信息

        my_logger = logging.getLogger()
        # my_logger.setLevel("DEBUG")
        formatter = logging.Formatter('%(asctime)s-%(levelname)s-%(filename)s-%(name)s-日志信息：%(message)s')

        # 创建输出一个渠道
        # my_handler = logging.StreamHandler()  # 控制台
        # my_logger.setLevel('DEBUG')
        # my_logger.addHandler(my_handler)

        my_file_handler = logging.FileHandler(log_path, encoding='utf-8')  # 文件输出
        # my_file_handler.setLevel('DEBUG')
        my_file_handler.setFormatter(formatter)
        my_logger.addHandler(my_file_handler)

        # 收集信息
        if level.upper() == "DEBUG":
            my_logger.setLevel("DEBUG")
            my_file_handler.setLevel('DEBUG')
            my_logger.debug(message)
        elif level.upper() == "INFO":
            my_logger.setLevel("INFO")
            my_file_handler.setLevel('INFO')
            my_logger.info(message)
        elif level.upper() == "WARING":
            my_logger.setLevel("WARING")
            my_file_handler.setLevel('WARING')
            my_logger.warning(message)
        elif level.upper() == "ERROR":
            my_logger.setLevel("ERROR")
            my_file_handler.setLevel('ERROR')
            my_logger.error(message)
        elif level.upper() == "CRITICAL":
            my_logger.setLevel("CRITICAL")
            my_file_handler.setLevel('CRITICAL')
            my_logger.critical(message)
        else:
            print('你输入的错误等级是错误的')
        # 关闭渠道
        my_logger.removeHandler(my_file_handler)

    def debug(self, msg):
        lg = Dolog()
        lg.my_logs(msg, 'DEBUG')

    def info(self, msg):
        lg = Dolog()
        lg.my_logs(msg, 'INFO')

    def warning(self, msg):
        lg = Dolog()
        lg.my_logs(msg, 'WARING')

    def error(self, msg):
        lg = Dolog()
        lg.my_logs(msg, 'ERROR')

    def critical(self, msg):
        lg = Dolog()
        lg.my_logs(msg, 'CRITICAL')


if __name__ == '__main__':
    Dolog().debug('你好厉害')
    Dolog().info('你好厉害')
