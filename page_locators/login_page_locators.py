# -*- utf-8 -*-
# @Time:2021/4/23 0023 18:54
# @Author: zhaowangfa
# @file login_page_locators.py
# 登录页面元素定位
from selenium.webdriver.common.by import By


class LoginPageLocators:
    # 元素定位
    # 用户名输入框
    name_input = (By.XPATH, '//input[@name="phone"]')
    # 密码输入框
    password_input = (By.XPATH, '//input[@name="password"]')
    # 记住密码按钮
    remember_user_bt = (By.XPATH, '//button[text()="记住密码"]')
    # 登录按钮
    login_button = (By.XPATH, '//button[text()="登录"]')
    # 错误提示框 -- 登录区域
    error_msg_from_loginAra = (By.XPATH, '//div[@class="form-error-info"]')
    # 免费注册按钮
    register_button = (By.XPATH, '//button[text()="免费注册"]')
    # 获取错误信息 - 页面正中间
    error_msg_from_center = (By.XPATH, '//div[@class="lay.layer.content"]')
