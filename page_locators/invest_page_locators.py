# -*- utf-8 -*-
# @Time:2021/4/24 0024 10:36
# @Author: zhaowangfa
# @file invest_page_locators.py
# 投资页面元素定位
from selenium.webdriver.common.by import By


class InvestPageLocators:
    # 元素定位
    # 投资金额输入框
    money_input = (By.XPATH, '')
    # 投资按钮
    invest_button = (By.XPATH, '')
