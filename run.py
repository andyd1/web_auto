# -*- utf-8 -*-
# @Time:2021/4/23 0023 10:19
# @Author: zhaowangfa
# @file run.py
# 运行测试用例
import unittest
import HTMLTestRunner
from test_case.test_login import TestLogin
from tools.get_path import *

suite = unittest.TestSuite()
loader = unittest.TestLoader()
suite.addTest(loader.loadTestsFromTestCase(TestLogin))

with open(test_report_path, 'wb') as file:
    runner = HTMLTestRunner.HTMLTestRunner(stream=file,
                                           verbosity=2,
                                           title='web自动化测试报告',
                                           description='duke执行的web测试',
                                           tester='duke')
    runner.run(suite)
