# -*- utf-8 -*-
# @Time:2021/4/23 0023 22:29
# @Author: zhaowangfa
# @file invest_page.py
# 投资页面
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from page_locators.invest_page_locators import InvestPageLocators as ioc


class InvestPage:

    def __init__(self, driver):
        self.driver = driver

    # 投资
    def invest(self):
        # 在输入框当中，输入金额
        WebDriverWait(self.driver, 30).until(ec.visibility_of_element_located(ioc.money_input))
        self.driver.find_element(*ioc.money_input).send_key('100')
        # 点击投标按钮
        self.driver.find_element(*ioc.invest_button).click()

    # 获取用户余额
    def get_user_money(self):
        WebDriverWait(self.driver, 30).until(ec.visibility_of_element_located(ioc.money_input))
        return self.driver.find_element(*ioc.money_input).get_attribute("data-amount")

    # 投资成功提示框 -- 点击查看并激活
    def click_invest_success_tips(self):
        pass

    # 错误提示框 - 页面中间
    def get_error_msg_center(self):
        # 获取文本内容
        # 关闭弹出框
        pass

    # 获取提示信息 -- 投资按钮上的
    def get_error_msg_inv_button(self):
        pass
