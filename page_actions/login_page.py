# -*- utf-8 -*-
# @Time:2021/4/22 0022 21:51
# @Author: zhaowangfa
# @file login_page.py
# 登录页

from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait
from page_locators.login_page_locators import LoginPageLocators as loc
from tools.base_page import BasePage


class LoginPage(BasePage):

    # 登录
    def login(self, username, passwd, remember_user=True):
        doc = '登录页面_正常登录'
        # WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located(loc.name_input))
        self.wait_ele_visible(loc.name_input, doc=doc)
        # self.driver.find_element(*loc.name_input).send_keys(username)
        self.input_text(loc.name_input, username, doc=doc)
        # self.driver.find_element(*loc.password_input).send_keys(passwd)
        self.input_text(loc.password_input, passwd, doc=doc)
        # 判断一下remember_user的值，来决定是否勾选
        if not remember_user:
            # self.driver.find_element(*loc.remember_user_bt).click()
            self.click_element(loc.remember_user_bt, doc=doc)
        # self.driver.find_element(*loc.login_button).click()
        self.click_element(loc.login_button, doc=doc)

    # 注册入口
    def register(self):
        # 免费注册
        WebDriverWait(self.driver, 20).until(ec.visibility_of_element_located(loc.register_button))
        self.driver.find_element(*loc.register_button).click()

    # 获取错误提示信息 - 登录区域
    def get_login_error_info(self):
        WebDriverWait(self.driver, 20)\
            .until(ec.visibility_of_element_located(loc.error_msg_from_loginAra))
        return self.driver.find_element(*loc.error_msg_from_loginAra).text

    # 获取错误信息 - 页面正中间
    def get_error_center_msg(self):
        WebDriverWait(self.driver, 20) \
            .until(ec.visibility_of_element_located(loc.error_msg_from_center))
        return self.driver.find_element(*loc.error_msg_from_center).text
