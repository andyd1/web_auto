# -*- utf-8 -*-
# @Time:2021/4/22 0022 21:51
# @Author: zhaowangfa
# @file index_page.py
# 首页
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
import random


class IndexPage:

    def __init__(self, driver):
        self.driver = driver

    def is_exit_logout_ele(self):
        try:
            WebDriverWait(self.driver, 20)\
                .until(ec.visibility_of_element_located((By.XPATH, '//a[@href="/Index/logout.html"]')))
            return True
        except Exception as e:
            print(e)
            return False

    # 选标操作  -- 默认选择第一个，元素定位时可以过滤掉不能投的标
    def click_first_bid(self):
        pass

    # 随机选标
    def rand_click_bid(self):
        elements = self.driver.find_elements(By.XPATH, '')
        # 随机数
        index = random.randint(0, len(elements) - 1)
        elements[index].click()
