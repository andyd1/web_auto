# -*- utf-8 -*-
# @Time:2021/4/23 0023 15:17
# @Author: zhaowangfa
# @file login_data.py
# 登录用例数据

# 正常登录
success_data = {"username": "15748477744", "password": "123456"}

# 异常登录，手机号码或密码不正确
error_phone = [{"username": "1574847774", "password": "123456", "check": "请输入正确的手机号码"},
               {"username": "157484777447", "password": "123456", "check": "请输入正确的手机号码"},
               {"username": "", "password": "123456", "check": "请输入手机号码"}]

# 异常登录，
error_data = [{"username": "12345678991", "password": "123456", "check": "此账号没有经过授权，请联系管理员"},
              {"username": "1574847774", "password": "123456密码", "check": "账号或密码错误"}
              ]
